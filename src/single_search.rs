/*
* The MIT License (MIT)
*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

use std::collections::HashMap;

use leptos::{create_local_resource, provide_context, Resource, RwSignal, SignalGet};
use meilisearch_sdk::{FacetStats, SearchResult, SearchResults};
use reqwest::Client;
use serde::de::DeserializeOwned;

use crate::{
    error::Error,
    search_query::{SearchQuery, SearchQueryBuilder, SearchQueryBuilderError},
    AuthParameters,
};

#[derive(Clone)]
pub struct SingleSearch<T>
where
    T: 'static + Clone + PartialEq + DeserializeOwned,
{
    parameters: AuthParameters,
    client: Client,
    index: String,
    search_query: RwSignal<SearchQuery>,
    resource: Resource<SearchQuery, Result<SearchResults<T>, Error>>,
}

impl<T> SingleSearch<T>
where
    T: 'static + Clone + PartialEq + DeserializeOwned,
{
    /// Initializes the `SingleSearch` instance and sets up the search environment.
    ///
    /// # Arguments
    ///
    /// - `parameters`: A `AuthParameters` struct containing authentication information.
    /// - `index`: The name of the Meilisearch index to use.
    /// - `search_query_builder`: A reference to a `SearchQueryBuilder` that defines the search query.
    ///
    /// # Returns
    ///
    /// Returns a `Result` indicating success or an error related to initializing the search.
    ///
    /// # Errors
    ///
    /// This can thrown an error if the `SearchQueryBuilder` could not build the `SearchQuery` successfully.
    pub fn init(
        parameters: AuthParameters,
        index: impl Into<String>,
        search_query_builder: &SearchQueryBuilder,
    ) -> Result<(), SearchQueryBuilderError> {
        let client = Client::new();
        let index = Into::<String>::into(index);
        let search_query = search_query_builder.build()?;
        let search_query = RwSignal::new(search_query);

        let resource = create_local_resource(move || search_query.get(), {
            let parameters = parameters.clone();
            let client = client.clone();
            let index = index.clone();
            move |search_query: SearchQuery| {
                execute_single_search_query(
                    parameters.clone(),
                    client.clone(),
                    index.clone(),
                    search_query,
                )
            }
        });

        provide_context(Self {
            parameters,
            client,
            index,
            search_query,
            resource,
        });

        Ok(())
    }

    /// Returns a read-write signal to the search query, allowing you to update the query parameters.
    ///
    /// # Returns
    ///
    /// Returns a `RwSignal` for the search query.
    #[must_use]
    pub fn search_query(&self) -> RwSignal<SearchQuery> {
        self.search_query
    }

    /// Retrieves the search results, which can be a successful result or an error.
    ///
    /// # Returns
    ///
    /// Returns an `Option` containing either the search results (`Ok`) or an error (`Err`). If
    /// no results are available, `None` is returned.
    #[must_use]
    pub fn get(&self) -> Option<Result<SearchResults<T>, Error>> {
        self.resource.get()
    }

    /// Retrieves the search results if they are successful. If there is an error or no results
    /// are available, this method returns `None`.
    ///
    /// # Returns
    ///
    /// Returns an `Option` containing the search results if they are successful (`Ok`), or `None`
    /// if an error occurred.
    pub fn ok(&self) -> Option<SearchResults<T>> {
        self.resource.get().and_then(Result::ok)
    }

    /// Retrieves the search error if the search operation resulted in an error. If the search
    /// was successful or no results are available, this method returns `None`.
    ///
    /// # Returns
    ///
    /// Returns an `Option` containing the search results if they are unsuccessful (`Ok`), or `None`
    /// if no error occurred.
    pub fn err(&self) -> Option<Error> {
        self.resource.get().and_then(Result::err)
    }

    /// Results of the query.
    #[must_use]
    pub fn hits(&self) -> Option<Vec<SearchResult<T>>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .map(|result| result.hits)
    }

    /// Number of documents skipped.
    #[must_use]
    pub fn offset(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.offset)
    }

    /// Number of results returned.
    #[must_use]
    pub fn limit(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.limit)
    }

    /// Estimated total number of matches.
    #[must_use]
    pub fn estimated_total_hits(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.estimated_total_hits)
    }

    // Current page number
    #[must_use]
    pub fn page(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.page)
    }

    // Maximum number of hits in a page.
    #[must_use]
    pub fn hits_per_page(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.hits_per_page)
    }

    // Exhaustive number of matches.
    #[must_use]
    pub fn total_hits(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.total_hits)
    }

    // Exhaustive number of pages.
    #[must_use]
    pub fn total_pages(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.total_pages)
    }

    /// Distribution of the given facets.
    #[must_use]
    pub fn facet_distribution(&self) -> Option<HashMap<String, HashMap<String, usize>>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.facet_distribution)
    }

    /// facet stats of the numerical facets requested in the `facet` search parameter.
    #[must_use]
    pub fn facet_stats(&self) -> Option<HashMap<String, FacetStats>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.facet_stats)
    }

    /// Processing time of the query.
    #[must_use]
    pub fn processing_time_ms(&self) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .map(|result| result.processing_time_ms)
    }

    /// Query originating the response.
    #[must_use]
    pub fn query(&self) -> Option<String> {
        self.resource
            .get()
            .and_then(Result::ok)
            .map(|result| result.query)
    }

    /// Index uid on which the search was made.
    #[must_use]
    pub fn index_uid(&self) -> Option<String> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|result| result.index_uid)
    }

    /// Create a new Resource which is independend to the resource from the
    /// given context. This is useful if you want to run a search query without
    /// manipulating your main context.
    #[must_use]
    pub fn create_resource(
        &self,
        search_query: RwSignal<SearchQuery>,
    ) -> Resource<SearchQuery, Result<SearchResults<T>, Error>> {
        create_local_resource(move || search_query.get(), {
            let parameters = self.parameters.clone();
            let client = self.client.clone();
            let index = self.index.clone();
            move |search_query: SearchQuery| {
                execute_single_search_query(
                    parameters.clone(),
                    client.clone(),
                    index.clone(),
                    search_query,
                )
            }
        })
    }
}

pub(crate) async fn execute_single_search_query<T: 'static + DeserializeOwned>(
    parameters: AuthParameters,
    client: Client,
    index: String,
    body: SearchQuery,
) -> Result<SearchResults<T>, Error> {
    let mut request = client.post(format!("{}/indexes/{}/search", parameters.host, index));
    if let Some(api_key) = parameters.api_key {
        request = request.bearer_auth(api_key.clone());
    }
    request
        .json(&body)
        .send()
        .await
        .map_err(|error| Error::ReqwestError(error.to_string()))?
        .json()
        .await
        .map_err(|error| Error::ReqwestError(error.to_string()))
}
