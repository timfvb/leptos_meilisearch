/*
* The MIT License (MIT)
*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

use std::collections::HashMap;

use leptos::{create_local_resource, provide_context, Resource, RwSignal, SignalGet};
use meilisearch_sdk::{FacetStats, MultiSearchResponse, SearchResult, SearchResults};
use reqwest::Client;
use serde::{de::DeserializeOwned, Serialize};

use crate::{
    error::Error,
    search_query::{SearchQuery, SearchQueryBuilderError},
    AuthParameters,
};

/// The `MultiSearchQuery` can contain multiple `SearchQuery`, it's important that the `index_uid` is set in the `SearchQuery`.
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Default, PartialEq, Serialize)]
pub struct MultiSearchQuery {
    pub queries: Vec<SearchQuery>,
}

/// A `MultiSearch` can execute multiple `SearchQuery` at once, usefull for a ecommerce search.
#[derive(Clone)]
pub struct MultiSearch<T>
where
    T: 'static + Clone + PartialEq + DeserializeOwned,
{
    parameters: AuthParameters,
    client: Client,
    search_queries: RwSignal<MultiSearchQuery>,
    resource: Resource<MultiSearchQuery, Result<MultiSearchResponse<T>, Error>>,
}

impl<T> MultiSearch<T>
where
    T: 'static + Clone + PartialEq + DeserializeOwned,
{
    /// Initializes the `Meilisearch` instance and sets up the search environment.
    ///
    /// # Arguments
    ///
    /// - `parameters`: A `AuthParameters` struct containing authentication information.
    ///
    /// # Returns
    ///
    /// Returns a `Result` indicating success or an error related to initializing the search.
    ///
    /// # Errors
    ///
    /// This can thrown an error if the `SearchQueryBuilder` could not build the `SearchQuery` successfully.
    pub fn init(parameters: AuthParameters) -> Result<(), SearchQueryBuilderError> {
        let client = Client::new();
        let search_queries = RwSignal::new(MultiSearchQuery::default());

        let resource = create_local_resource(move || search_queries.get(), {
            let parameters = parameters.clone();
            let client = client.clone();
            move |multi_search_query: MultiSearchQuery| {
                execute_multi_search_query(parameters.clone(), client.clone(), multi_search_query)
            }
        });

        provide_context(Self {
            parameters,
            client,
            search_queries,
            resource,
        });

        Ok(())
    }

    /// Returns a read-write signal to the search query, allowing you to update the query parameters.
    ///
    /// # Returns
    ///
    /// Returns a `RwSignal` for the search query.
    #[must_use]
    pub fn search_queries(&self) -> RwSignal<MultiSearchQuery> {
        self.search_queries
    }

    /// Retrieves the search results, which can be a successful result or an error.
    ///
    /// # Returns
    ///
    /// Returns an `Option` containing either the search results (`Ok`) or an error (`Err`). If
    /// no results are available, `None` is returned.
    #[must_use]
    pub fn get(&self) -> Option<Result<MultiSearchResponse<T>, Error>> {
        self.resource.get()
    }

    /// Retrieves the search results if they are successful. If there is an error or no results
    /// are available, this method returns `None`.
    ///
    /// # Returns
    ///
    /// Returns an `Option` containing the search results if they are successful (`Ok`), or `None`
    /// if an error occurred.
    pub fn ok(&self) -> Option<MultiSearchResponse<T>> {
        self.resource.get().and_then(Result::ok)
    }

    /// Retrieves the search error if the search operation resulted in an error. If the search
    /// was successful or no results are available, this method returns `None`.
    ///
    /// # Returns
    ///
    /// Returns an `Option` containing the search results if they are unsuccessful (`Ok`), or `None`
    /// if no error occurred.
    pub fn err(&self) -> Option<Error> {
        self.resource.get().and_then(Result::err)
    }

    /// Results of the query.
    #[must_use]
    pub fn results(&self) -> Option<Vec<SearchResults<T>>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .map(|response| response.results)
    }

    /// Results of the query.
    #[must_use]
    pub fn hits(&self, index: usize) -> Option<Vec<SearchResult<T>>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .map(|result| result.hits.clone())
            })
    }

    /// Number of documents skipped.
    #[must_use]
    pub fn offset(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| response.results.get(index).and_then(|result| result.offset))
    }

    /// Number of results returned.
    #[must_use]
    pub fn limit(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| response.results.get(index).and_then(|result| result.limit))
    }

    /// Estimated total number of matches.
    #[must_use]
    pub fn estimated_total_hits(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.estimated_total_hits)
            })
    }

    // Current page number
    #[must_use]
    pub fn page(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| response.results.get(index).and_then(|result| result.page))
    }

    // Maximum number of hits in a page.
    #[must_use]
    pub fn hits_per_page(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.hits_per_page)
            })
    }

    // Exhaustive number of matches.
    #[must_use]
    pub fn total_hits(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.total_hits)
            })
    }

    // Exhaustive number of pages.
    #[must_use]
    pub fn total_pages(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.total_pages)
            })
    }

    /// Distribution of the given facets.
    #[must_use]
    pub fn facet_distribution(
        &self,
        index: usize,
    ) -> Option<HashMap<String, HashMap<String, usize>>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.facet_distribution.clone())
            })
    }

    /// facet stats of the numerical facets requested in the `facet` search parameter.
    #[must_use]
    pub fn facet_stats(&self, index: usize) -> Option<HashMap<String, FacetStats>> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.facet_stats.clone())
            })
    }

    /// Processing time of the query.
    #[must_use]
    pub fn processing_time_ms(&self, index: usize) -> Option<usize> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .map(|result| result.processing_time_ms)
            })
    }

    /// Query originating the response.
    #[must_use]
    pub fn query(&self, index: usize) -> Option<String> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .map(|result| result.query.clone())
            })
    }

    /// Index uid on which the search was made.
    #[must_use]
    pub fn index_uid(&self, index: usize) -> Option<String> {
        self.resource
            .get()
            .and_then(Result::ok)
            .and_then(|response| {
                response
                    .results
                    .get(index)
                    .and_then(|result| result.index_uid.clone())
            })
    }

    /// Create a new Resource which is independend to the resource from the
    /// given context. This is useful if you want to run a search query without
    /// manipulating your main context.
    #[must_use]
    pub fn create_resource(
        &self,
        search_query: RwSignal<MultiSearchQuery>,
    ) -> Resource<MultiSearchQuery, Result<MultiSearchResponse<T>, Error>> {
        create_local_resource(move || search_query.get(), {
            let parameters = self.parameters.clone();
            let client = self.client.clone();
            move |search_query: MultiSearchQuery| {
                execute_multi_search_query(parameters.clone(), client.clone(), search_query)
            }
        })
    }
}

pub(crate) async fn execute_multi_search_query<T: 'static + DeserializeOwned>(
    parameters: AuthParameters,
    client: Client,
    body: MultiSearchQuery,
) -> Result<MultiSearchResponse<T>, Error> {
    let mut request = client.post(format!("{}/multi-search", parameters.host));
    if let Some(api_key) = parameters.api_key.clone() {
        request = request.bearer_auth(api_key);
    }
    request
        .json(&body)
        .send()
        .await
        .map_err(|error| Error::ReqwestError(error.to_string()))?
        .json()
        .await
        .map_err(|error| Error::ReqwestError(error.to_string()))
}
